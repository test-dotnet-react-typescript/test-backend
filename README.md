# Your .NET Project Name

## Introduction

This is a .NET project that demonstrates how to create a web application with a .NET backend using Entity Framework Core, and how to interact with a PostgreSQL database using Docker. The project includes steps to set up a PostgreSQL database container, start the .NET project, and add initial mock data via Swagger.

## Prerequisites

Before you begin, ensure you have the following installed:

- [.NET SDK](https://dotnet.microsoft.com/download)
- [Docker](https://www.docker.com/get-started)
- [PostgreSQL Client](https://www.postgresql.org/download/)

## Getting Started

### Step 1: Set Up the PostgreSQL Database

To set up a PostgreSQL database container, run the following command:

```bash
docker run -d \
  --name postgres-container \
  -e POSTGRES_DB=test_db \
  -e POSTGRES_USER=root \
  -e POSTGRES_PASSWORD=root \
  -p 5432:5432 \
  postgres:latest
```

This command will create a Docker container named "postgres-container" with a PostgreSQL database, ready to be used by your .NET application.

### Step 2: Start the .NET Project

To start the .NET project, follow these steps:

1.  Open a terminal window.
2.  Navigate to the root directory of your .NET project.
3.  Run the following command to build the project:

bash

```bash
dotnet build
```

or if you want to watch for changes and run the project:

bash

```bash
dotnet watch run
```

This will build and start your .NET project.

### Step 3: Add Initial Mock Data via Swagger

1.  Once your .NET project is running, open a web browser and navigate to the Swagger documentation page. By default, it should be available at `http://localhost:5000/swagger/index.html`.
2.  Use the Swagger interface to add entities to the database. These entities will serve as initial mock data for your application.

Usage
-----

*   Access your .NET application at `http://localhost:5000`.
*   Use the Swagger documentation at `http://localhost:5000/swagger/index.html` to interact with the API and manage your database entities.