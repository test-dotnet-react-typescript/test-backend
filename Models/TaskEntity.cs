using System;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace TestDotnetBackend.Models
{
    public class TaskEntity
    {
        [BindNever]
        public Guid Id { get; set; }
        public string? Title { get; set; }
        public string? Description { get; set; }
        public DateTime DueDate { get; set; }
        public bool IsCompleted { get; set; }
    }
}