using Microsoft.EntityFrameworkCore;
using TestDotnetBackend.Models; // add this using directive

namespace TestDotnetBackend
{
    public class TestDbContext : DbContext
    {
        public TestDbContext(DbContextOptions<TestDbContext> options) : base(options)
        {
        }

        public DbSet<TaskEntity> Tasks { get; set; }
    }
}
