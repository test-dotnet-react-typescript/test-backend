using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;
using TestDotnetBackend.Models;
using System.Collections.Generic;
using TestDotnetBackend.DTOs;
using Microsoft.Extensions.Caching.Memory;

namespace TestDotnetBackend.Controllers
{

    [ApiController]
    [Route("[controller]")]
    public class TaskController : ControllerBase
    {
        private readonly ILogger<TaskController> _logger;
        private readonly TestDbContext _dbContext;

        private readonly IMemoryCache _memoryCache;
        private const string CacheKey = "TasksCache";

        public TaskController(TestDbContext dbContext, IMemoryCache memoryCache)
        {
            _dbContext = dbContext;
            _memoryCache = memoryCache;
        }

        [HttpGet]
        public IActionResult GetTasks()
        {

            if (!_memoryCache.TryGetValue(CacheKey, out List<TaskEntity> tasks))
            {
                // If not found in the cache, initialize with the dummy data from the database
                tasks = _dbContext.Tasks.ToList();
                _memoryCache.Set(CacheKey, tasks);
            }

            return Ok(tasks);
        }

        [HttpPost]
        public IActionResult UpdateTasks([FromBody] List<TaskEntity> updatedTasks)
        {
            _memoryCache.Set(CacheKey, updatedTasks);

            return Ok(updatedTasks);
        }






        [HttpGet("all", Name = "GetAllTasks")]
        [SwaggerOperation(Tags = new[] { "Task - Standard CRUD DB Operations" })]
        public IActionResult GetAllTasks()
        {
            var tasks = _dbContext.Tasks.ToList();
            if (tasks == null)
            {
                return NotFound();
            }
            return Ok(tasks);
        }

        [HttpGet("{id}", Name = "GetTaskById")]
        [SwaggerOperation(Tags = new[] { "Task - Standard CRUD DB Operations" })]
        public IActionResult GetTaskById(int id)
        {
            var task = _dbContext.Tasks.Find(id);
            if (task == null)
            {
                return NotFound();
            }
            return Ok(task);
        }

        [HttpPost("create", Name = "CreateTask")]
        [SwaggerOperation(Tags = new[] { "Task - Standard CRUD DB Operations" })]
        public IActionResult CreateTask(TaskCreateDTO taskRequest)
        {
            var task = new TaskEntity
            {
                Title = taskRequest.Title,
                Description = taskRequest.Description,
                DueDate = taskRequest.DueDate,
                IsCompleted = taskRequest.IsCompleted
            };
            _dbContext.Tasks.Add(task);
            _dbContext.SaveChanges();
            return CreatedAtRoute(nameof(GetTaskById), new { Id = task.Id }, task);
        }

        [HttpPut("update/{id}", Name = "UpdateTask")]
        [SwaggerOperation(Tags = new[] { "Task - Standard CRUD DB Operations" })]
        public IActionResult UpdateTask(int id, TaskEntity task)
        {
            var taskToUpdate = _dbContext.Tasks.Find(id);
            if (taskToUpdate == null)
            {
                return NotFound();
            }
            taskToUpdate.Title = task.Title;
            taskToUpdate.Description = task.Description;
            taskToUpdate.DueDate = task.DueDate;
            taskToUpdate.IsCompleted = task.IsCompleted;
            _dbContext.SaveChanges();
            return NoContent();
        }

        [HttpDelete("delete/{id}", Name = "DeleteTask")]
        [SwaggerOperation(Tags = new[] { "Task - Standard CRUD DB Operations" })]
        public IActionResult DeleteTask(int id)
        {
            var taskToDelete = _dbContext.Tasks.Find(id);
            if (taskToDelete == null)
            {
                return NotFound();
            }
            _dbContext.Tasks.Remove(taskToDelete);
            _dbContext.SaveChanges();
            return NoContent();
        }
    }
}

